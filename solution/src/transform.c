#include "../include/transform.h"

struct image rotate( struct image const source ) {
   if ( source.data == NULL ) {
       return (struct image){0};
   }
   struct image new_image = create_image(source.height, source.width);
   for (uint64_t i = 0; i < source.height; i++) {
       for (uint64_t j = 0; j < source.width; j++) {
           new_image.data[source.height * j + source.height-i-1] = source.data[source.width * i + j];
       }
   }
   return new_image;
}
