#include "../include/bmp.h"

enum read_status read_pixels( FILE *in, struct bmp_header *header, struct image *img ) {
   *img = create_image(header->biWidth, header->biHeight);
   if ( !img->data ) {
      return READ_INVALID_BITS;
   }
   for ( uint32_t i = 0; i < img->height; ++i ) {
      if ( fread(img->data + (i * img->width), sizeof(struct pixel), img->width, in) != img->width ) {
         destroy_image(img);
         return READ_INVALID_SIGNATURE;   
      }
      if ( fseek(in, padding(img->width), SEEK_CUR) != 0 ) {
         destroy_image(img);
         return READ_INVALID_SIGNATURE;   
      }
   }
   return READ_OK;
}

enum read_status from_bmp( FILE *in, struct image *img ) {
   struct bmp_header header = {0};
   if ( !fread(&header, sizeof(struct bmp_header), 1, in) ) {
      destroy_image(img);
      return READ_INVALID_HEADER;
   }
   if ( header.biWidth <= 0 || header.biHeight <= 0 ) {
      destroy_image(img);
      return READ_INVALID_HEADER;
   }
   if ( header.bfType != 0x4D42 ) {
      destroy_image(img);
      return READ_INVALID_SIGNATURE;
   }
   return read_pixels(in, &header, img);
}

enum write_status write_pixels( FILE *out, const struct image *img ) {
    for ( uint32_t i = 0; i < img->height; ++i ) {
        if ( fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out) != img->width ) {
            destroy_image(img);
            return WRITE_ERROR;
        }
        if ( fseek(out, padding(img->width), SEEK_CUR) != 0 ) {
            destroy_image(img);
            return WRITE_ERROR;   
        }
    }
    return WRITE_OK;
}

enum write_status to_bmp( FILE *out, const struct image *img ) {
   struct bmp_header header = create_header(img);
   if (!fwrite(&header, sizeof(struct bmp_header), 1, out)) {
       destroy_image(img);
       return WRITE_ERROR;
   } 
   return write_pixels(out, img);
   
}
