#include "../include/file.h"

enum file_status open_file( FILE** file, const char* file_name, const char* mode ) {
   *file = fopen( file_name, mode );
   if (!*file) {
       return ERROR;
   }
   else {
       return SUCCESS;
   }
}

enum file_status close_file( FILE** file ) {
   if (fclose(*file) == EOF) {
       return ERROR;
   }
   else {
       return SUCCESS;
   }
}
