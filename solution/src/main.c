#include "../include/bmp.h"
#include "../include/file.h"
#include "../include/transform.h"

#include <stdbool.h>

void program_usage(void) {
    printf("Command: ./build/image-transformer <BMP read file path> <BMP write file path>\n");
}

bool close_files(FILE *in, FILE *out) {
    if ( close_file(&in) == ERROR || close_file(&out) == ERROR ) {
        fprintf(stderr, "Closing file failed\n");
        return false;
    }
    return true;
}

int main( int argc, char** argv ) {
    if ( argc != 3 ) {
        program_usage();
        return -1;
    }
    FILE *in;
    FILE *out;
    if ( open_file(&in, argv[1], "rb" ) == ERROR ) {
        fprintf(stderr, "Opening file for reading failed\n");
        return -1;
    }
    if ( open_file(&out, argv[2], "wb" ) == ERROR ) {
        fprintf(stderr, "Opening file for writing failed\n");
        return -1;
    }
    printf("Files opened successfully\n");
    struct image img = {0};
    if ( from_bmp(in, &img) != READ_OK ) {
        fprintf(stderr, "Reading file failed\n");
        close_files(in, out);
        return -1;
    }
    printf("Image readed from file successfully\n");
    struct image new = rotate(img);
    if ( to_bmp(out, &new) != WRITE_OK ) {
        fprintf(stderr, "Writing file failed\n");
        close_files(in, out);
        return -1;
    }
    printf("Image transformed and written to file successfully\n");
    if ( close_files(in, out) == false ) {
        destroy_image(&img);
        destroy_image(&new);
        return -1;
    }
    destroy_image(&img);
    destroy_image(&new);
    return 0;
}
