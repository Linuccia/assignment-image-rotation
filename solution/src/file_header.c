#include "../include/file_header.h"

struct bmp_header create_header( const struct image *img ) {
    return (struct bmp_header) {
            .bfType = 0x4D42,
            .bfileSize = sizeof(struct bmp_header) + image_size(img),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = image_size(img),
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
}
