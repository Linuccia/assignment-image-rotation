#include "../include/img.h"

struct image create_image( uint64_t width, uint64_t height ) {
   struct image img = {0};
   img.width = width;
   img.height = height;
   img.data = malloc(width * height * sizeof(struct pixel));
   return img;
}

void destroy_image(const struct image* img ) {
   free(img->data);
}


uint32_t padding( uint32_t width ) {
    return (4 - ((width * sizeof(struct pixel)) % 4));
}

uint32_t image_size( const struct image *img ) {
    return (sizeof(struct pixel) * img->width + padding(img->width)) * img->height;
}

