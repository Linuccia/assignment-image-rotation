#include <stdio.h>

enum file_status {
   SUCCESS,
   ERROR
};

enum file_status open_file( FILE** file, const char* file_name, const char* mode );

enum file_status close_file( FILE** file );
