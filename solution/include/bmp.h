#include "file_header.h"

#include <stdio.h>

/*  deserializer   */
enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER
  /* коды других ошибок  */
  };
  
enum read_status read_pixels( FILE *in, struct bmp_header *header, struct image *img );

enum read_status from_bmp( FILE* in, struct image* img );

/*  serializer   */
enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR
  /* коды других ошибок  */
};

enum write_status write_pixels( FILE *out, const struct image *img );

enum write_status to_bmp( FILE* out, struct image const* img );

