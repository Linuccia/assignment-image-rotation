#include "img.h"

#include <stddef.h>

/* создаёт копию изображения, которая повёрнута на 90 градусов против часовой стрелки */
struct image rotate( struct image const source );
