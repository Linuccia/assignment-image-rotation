#include <stdint.h>
#include <stdlib.h>

#pragma once

struct pixel { uint8_t b, g, r; };

struct image {
  uint64_t width, height;
  struct pixel* data;
};

struct image create_image( uint64_t width, uint64_t height );

void destroy_image( const struct image* img );
  
uint32_t padding( uint32_t width );

uint32_t image_size( const struct image *img );

